program n2
real:: x, y, r, theta 
write(*,*) 'Введите X'
read(*,*) x
write(*,*) 'Введите Y'
read(*,*) y
r = sqrt(x**2 + y**2)
write(*,*) 'r =', r
if ((x == 0) .and. (y == 0)) then
    theta = 0
    write(*,*) 'theta =', theta
else if ((x > 0) .and. (y >= 0)) then
    theta = atan2(y, x)
    write(*,*) 'theta =',theta
else if ((x == 0) .and. (y > 0)) then
    write(*,*) 'theta = pi/2'
else if (x < 0) then
    theta = atan2(y, x)
    write(*,*) 'theta = pi +',theta
else if ((x == 0) .and. (y < 0)) then
    write(*,*) 'theta = 3pi/2' 
else if ((x > 0) .and. (y < 0)) then
    theta = atan2(y, x)
    write(*,*) 'theta = 2pi +',theta 
end if
end program n2