program n3
implicit none
integer :: n, i, m
real, allocatable :: P(:), md(:) ! массив с коэффициентами полинома
real:: a0, gran, B
read(*, *) n ! ввод

allocate(P(n + 1)) ! выделение памяти
P = readArray(n + 1) ! функция считывает массив с клавиатуры и возвращает его
!описано в разделе contains
call writeArray(P, n + 1) !процедура вывода массива

m = -1
do i = 1, n
    if ((P(i) < 0) .and. (m == -1)) then
        m = i - 1
        b = abs(P(i))
    else if ((P(i) < 0) .and. (abs(P(i)) > b)) then
        b = abs(P(i))
    end if
end do
a0 = P(1)
!print *, md
!print *, B, a0, m
gran = 1.0 + (B/a0)**(1/m)
write(*, '(F6.2, A, F6.2)') 0.0, " <= |x| <= ", gran


deallocate(P) ! очистка памяти

contains

function readArray(n) result(arr)
implicit none
integer, intent(in) :: n
real :: arr(n)

integer :: i

do i = 1, n
write(*, '(A, I2, A)', advance="no") "a", i - 1, ":"
read(*, *) arr(i)
end do
end function

subroutine writeArray(arr, n)
integer, intent(in) :: n
real :: arr(n)

integer :: i

do i = 1, n
write(*, '(F6.2)', advance="no") arr(i)
enddo
print*
end subroutine

end program n3