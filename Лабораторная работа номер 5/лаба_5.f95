program n5

real, parameter :: pi = 3.141593
real xk, x0, h, e, x, t, u, S, y, fact 
integer nmax, imax, i, ni

x0 = 0
xk = 1.
h = 0.1
nmax = 100
e = 0.0000001

write(*, "(a)")" ==============================================="

write(*, "(a)")"    Исходные данные:"
write(*, "((a)1x(f5.2)1x(a)1x(f5.2)1x(a)1x(f5.2))")" x0=", x0, "xk=", xk, "h=", h
write(*, "((a)1x(e8.3)1x(a)1x(i3))")" погрешность=", e, "nmax=", nmax
write(*, "(a)")" -----------------------------------------------"

write(*, "(a)")" Таблица приближения:"
write(*, "(a)")"+-----+-------+-----+-------+---------+---------+"
write(*, "(a)")"|  i  |   x   |  n  | Func  |  error  |    y    |"
write(*, "(a)")"+-----+-------+-----+-------+---------+---------+"

imax = ((xk - x0) / h + 0.5) + 1
do i = 1, imax
    x = x0 + (i - 1) * h
    if (abs(x) <= 1) then
        y = F(x)
        write(*, "(a(i5)a(f7.2)a(i5)a(f7.2)a(e8.3)a(f7.2)a)")'|', i, '|', x, '|', ni, '|', y, '|', abs(cos(x) - y), &
' |', (cos(x)),'  |'  ! сравниваем с истинной Func и выводим ошибку в таблицу
    end if

        write(*, "(a)")"+-----+-------+-----+-------+---------+---------+"
end do
write(*, "(a)")" ================================="

contains
real function F(x) result(sn)
real:: s, u, e, real
real, parameter :: pi = 3.141593
integer::  n, nmax
nmax = 100
e = 0.0000001
do n = 1, nmax
        if (n == 1) then
            fact = 1
        else
            fact = fact * (2*(n) - 1) * (2*(n) - 2)
        end if
        u = (-1)**n * (x - pi/2)**(2*n - 1) / fact
        s = u + s
        if ((abs(u)) <= (e * abs(S))) then
            ni = n
            sn = s
            exit
        end if
end do
end function F

end program n5