program n4
integer:: n, i
real:: x, y, p, e
i = 0
n = 0
y = 0.3
p = 3.001768
x = 1 + p**2
e = 10e-7

nmax = 100

do i = 1, nmax
    if (abs((x*(y**2) - 1)) > e) then
        y = y*(3 - x*(y**2))/2
        n = i
    else
        exit
    endif
end do
write(*,*) n, y
print *, 1 / sqrt(1+p**2)
end program n4
