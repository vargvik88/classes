program n6
  implicit none
  integer :: n, i, j
  integer, dimension(5,5) :: a
  integer, dimension(5) :: b
  
  ! Считываем матрицу с клавиатуры
  print *, "Введите матрицу размером 5x5:"
  do i = 1, 5
    read *, (a(i,j), j = 1, 5)
  end do
  
  ! Вычисляем количество уникальных элементов в каждой строке
  do i = 1, 5
    b(i) = unique_count(a(i,:))
  end do
  
  ! Выводим вектор b на экран
  print *, "Вектор b:"
  write (*, "(5i4)") b
  
contains

  ! Функция для вычисления количества уникальных элементов в массиве
  function unique_count(arr) result(count)
    integer, dimension(:), intent(in) :: arr
    integer :: count, i, j
    logical :: is_unique
    integer, dimension(size(arr)) :: unique
    
    count = 0
    do i = 1, size(arr)
      is_unique = .true.
      do j = 1, count
        if (arr(i) == unique(j)) then
          is_unique = .false.
          exit
        end if
      end do
      if (is_unique) then
        count = count + 1
        unique(count) = arr(i)
      end if
    end do
  end function unique_count

end program n6